﻿module Adventure.Movement
open Adventure.Utilities
open Adventure.Characters
open Adventure.Map
open Adventure.GameState
open Adventure.Attack
type Direction = | Up | Down | Left | Right
type DirectionOption = Direction option
let parseDirection str =
    match str with
    | "up" -> Some Up
    | "down" -> Some Down
    | "left" -> Some Left
    | "right" -> Some Right
    |_ -> None
    
(* Move validation *)
let canMoveDistance x1 y1 x2 y2 =
    let moveDistance = calcDistance x1 y1 x2 y2
    moveDistance > 0 && moveDistance < 2
let canMoveToDestination state character destX destY =
    let legalDistance =
        match character.Position with
        | x,y -> canMoveDistance x y destX destY
    let spaceIsClear =
        match checkSpace state destX destY with
        | Floor -> true
        | _ -> false
    legalDistance && spaceIsClear

(* Generic movement *)
let moveCharacter state character destX destY =
    let movedCharacter = changePosition character destX destY
    updateCharacters state movedCharacter
let tryMove character destX destY state  =
    let canMove = canMoveToDestination state character destX destY
    match canMove with
    | true -> moveCharacter state character destX destY
    | false ->
        match checkSpace state destX destY with
        | Wall -> state
        | Character -> tryAttack state character destX destY
        | Player -> tryAttack state character destX destY
        | _ -> state
let tryMoveUp character state =
    match character.Position with
    | x,y -> tryMove character x (y - 1) state
let tryMoveDown character state =
    match character.Position with
    | x,y -> tryMove character x (y + 1) state
let tryMoveLeft character state =
    match character.Position with
    | x,y -> tryMove character (x - 1) y state
let tryMoveRight character state =
    match character.Position with
    | x,y -> tryMove character (x + 1) y state

(* Player Movement *)
let playerTryMoveUp state =
    let character = state.Characters |> List.find isPlayer
    tryMoveUp character state
let playerTryMoveDown state =
    let character = state.Characters |> List.find isPlayer
    tryMoveDown character state
let playerTryMoveLeft state =
    let character = state.Characters |> List.find isPlayer
    tryMoveLeft character state
let playerTryMoveRight state =
    let character = state.Characters |> List.find isPlayer
    tryMoveRight character state
let movePlayer direction state =
    match direction with
    | Up -> playerTryMoveUp state
    | Down -> playerTryMoveDown state
    | Left -> playerTryMoveLeft state
    | Right -> playerTryMoveRight state
let movePlayerWithString (str: string) state =
    let dir = str.ToLower() |> parseDirection
    match dir with
    | Some d -> movePlayer d state
    | None -> state