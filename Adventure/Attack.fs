﻿module Adventure.Attack
open Adventure.Utilities
open Adventure.Characters
open Adventure.GameState
let canAttackDistance character destX destY =
    match character.Position with
    | x,y -> (calcDistance x y destX destY) <= character.Reach
let tryAttack state character destX destY =
    let inRange = canAttackDistance character destX destY
    match inRange with
    | false -> state
    | true -> 
        let characterUnderAttack = getCharacterAtSpace state destX destY
        let damagedCharacter = attack character characterUnderAttack
        updateCharacters state damagedCharacter    