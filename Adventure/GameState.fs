﻿module Adventure.GameState
open Adventure.Characters
open Adventure.Map
type State =
    {
        Characters: Character list
        mutable Board: Tile array array
        Time: int
    }
let clearTile tile =
    match tile with
    | Character -> Floor
    | Player -> Floor
    | _ -> tile
let insertIntoBoard (board: Tile array array) character =
    let mutable newBoard = board
    match character.Position with
    | x,y ->
        match character.Player with
        | true -> newBoard.[y].[x] <- Player
        | false -> newBoard.[y].[x] <- Character
    newBoard
let clearTileRow row = row |> Array.map clearTile
let cleanBoard board = board |> Array.map clearTileRow
let makeState characters board time =
    let newBoard = cleanBoard board
    let filledBoard = (newBoard, characters) ||> List.fold insertIntoBoard
    {Characters = characters; Board = filledBoard; Time = time + 1}
let checkSpace state x y = state.Board.[y].[x]
let characterIsAt x y character =
    match character.Position with
    | cx,cy -> cx = x && cy = y
let getCharacterAtSpace state x y =
    let characterIsAtThisSpace = characterIsAt x y
    state.Characters |> List.find characterIsAtThisSpace
let updateCharacters state character =
    let newCharacters =
        state.Characters |> List.map (fun c ->
            match isSameCharacter character c with
            | true -> character
            | false -> c
            )
    makeState newCharacters state.Board state.Time
