﻿module Adventure.Species

type public Species =
    | Goblin
    | Orc
    | Human