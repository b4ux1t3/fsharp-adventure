﻿module Adventure.FileIo

open System.IO

let readFile filename = File.ReadLines filename |> List.ofSeq
