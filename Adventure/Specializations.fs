﻿module Adventure.Specializations

type Specialization =
    | Wizard
    | Warrior
    | Thief