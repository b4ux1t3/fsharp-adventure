module Adventure.Main

open System
open Adventure.Utilities
open Adventure.Specializations
open Adventure.Species
open Adventure.Characters
open Adventure.Abilities
open Adventure.GameState
open Adventure.Attack
open Adventure.Movement
open Adventure.FileIo

// get command line args
let mapFile = Environment.GetCommandLineArgs().[1]
//let characterFile = Environment.GetCommandLineArgs().[2]

// load file, or generate
let initialBoard =
    let mapString = readFile mapFile
    match Map.mapIsValid mapString with
    | true -> Map.parseLines mapString
    | false -> Map.defaultMap

printfn $"%s{Map.mapBoardToDisplay initialBoard}"
let player = {
    Name="Steve"
    Specialization=Warrior
    Species=Human
    Attack=4
    Defense=1
    Reach=1
    MaxHealth=10
    CurrentHealth=10
    Sneak=10
    Position=(5,4)
    SpecialAbilityCost=1
    SpecialAbilityGain=5
    Player=true
}

let initialState = makeState [player] initialBoard 0

printfn $"%s{Map.mapBoardToDisplay initialState.Board}"

let firstStep = playerTryMoveUp initialState

// Player should not have moved
printfn $"%s{Map.mapBoardToDisplay firstStep.Board}"

let secondStep = playerTryMoveRight firstStep

// Player should have moved to the right.
printfn $"%s{Map.mapBoardToDisplay secondStep.Board}"

let thirdStep = playerTryMoveRight secondStep

// Player should have moved to the right again.
printfn $"%s{Map.mapBoardToDisplay thirdStep.Board}"

let fourthStep = playerTryMoveUp thirdStep

// Player should have moved to the right again.
printfn $"%s{Map.mapBoardToDisplay fourthStep.Board}"


// read, eval, play, loop
//  - Read input from user
//  - Evaluate game state
//  - Generate new state
//  - Repeat
